# DevTools Demo MacOsX only

+ jcef wurde mit Java 1.7.0.79 gebaut
+ Bitte externe Lib(s) auf richtigen Pfad (./libs/jcef_app.app/Contents/Java) anpassen, da IntelliJ wohl keine relativen Pfade zulässt

+ Konsolenausgabe:
```
#!Java

initialize on Thread[AWT-EventQueue-0,6,main] with library path ./Contents/Java
[0526/101250:ERROR:renderer_main.cc(207)] Running without renderer sandbox
[0526/101254:ERROR:renderer_main.cc(207)] Running without renderer sandbox
[0526/101254:ERROR:content_client.cc(148)] No data resource available for id 22001
[0526/101254:WARNING:url_request_job_manager.cc(101)] Failed to map: chrome-devtools://devtools/devtools.html
[0526/101254:ERROR:CONSOLE(1)] "Uncaught ReferenceError: InspectorFrontendAPI is not defined", source:  (1)
```