/**
 * Created by dominiceubel on 26.05.15.
 */
public class Main
{
    public static void main(String[] args)
    {
        JCefBrowser jCefBrowser = new JCefBrowser();
        jCefBrowser.setSize(1024, 768);
        jCefBrowser.setVisible(true);
    }
}
