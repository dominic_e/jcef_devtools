import org.cef.browser.CefBrowser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Created by dominiceubel on 26.05.15.
 */
public class DevTools extends JDialog
{
    private final CefBrowser devTools;

    public DevTools(CefBrowser cefBrowser)
    {
        devTools = cefBrowser.getDevTools();
        getContentPane().add(devTools.getUIComponent(), BorderLayout.CENTER);

        addComponentListener(new ComponentAdapter()
        {
            @Override
            public void componentHidden(ComponentEvent e)
            {
                dispose();
            }
        });
    }
}
