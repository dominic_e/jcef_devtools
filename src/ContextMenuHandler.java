import org.cef.browser.CefBrowser;
import org.cef.callback.CefContextMenuParams;
import org.cef.callback.CefMenuModel;
import org.cef.handler.CefContextMenuHandlerAdapter;

/**
 * Created by dominiceubel on 26.05.15.
 */
public class ContextMenuHandler extends CefContextMenuHandlerAdapter
{
    public ContextMenuHandler()
    {

    }

    @Override
    public void onBeforeContextMenu(CefBrowser browser, CefContextMenuParams params, CefMenuModel model)
    {
        //super.onBeforeContextMenu(browser, params, model);
        if(model.getCount() > 0)
            model.clear();

        model.addItem(100, "Open DevTools");
    }

    @Override
    public boolean onContextMenuCommand(CefBrowser browser, CefContextMenuParams params, int commandId, int eventFlags)
    {
        //return super.onContextMenuCommand(browser, params, commandId, eventFlags);

        switch(commandId)
        {
            case 100: //DevTools
                DevTools devTools = new DevTools(browser);
                devTools.setSize(800, 600);
                devTools.setVisible(true);
                break;
        }
        return true;
    }
}
