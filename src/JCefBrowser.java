import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.CefSettings;
import org.cef.OS;
import org.cef.browser.CefBrowser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by dominiceubel on 26.05.15.
 */
public class JCefBrowser extends JFrame
{
    private final CefBrowser cefBrowser;
    private final CefApp cefApp;
    private final CefClient cefClient;
    private final CefSettings cefSettings;

    public JCefBrowser() throws HeadlessException
    {
        cefSettings = new CefSettings();

        //cefSettings.resources_dir_path = "./Contents/Frameworks/Chromium Embedded Framework.framework/Resources";

        cefApp = CefApp.getInstance(cefSettings);
        cefClient = cefApp.createClient();

        cefClient.addContextMenuHandler(new ContextMenuHandler());

        cefBrowser = cefClient.createBrowser("www.google.de", OS.isLinux(), false);

        getContentPane().add(cefBrowser.getUIComponent(), BorderLayout.CENTER);

        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                dispose();
                cefApp.dispose();
                System.exit(0);
            }
        });
    }
}
